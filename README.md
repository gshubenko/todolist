
# TodoList

Rest API that allows you to create todo, assign it, create comment to task and attach images to tasks. Also users get emails after task created and updated.

* `/api/v1/redoc Link to doc`

## 📜 Table of contents

1. [Requirements and Preparation](https://github.com/ShubenkoH/weather-api/blob/main/README.md#electric_plug-requirements-and-preparation)
2. [Installation](https://github.com/ShubenkoH/weather-api/blob/main/README.md#hammer_and_wrench-installation)
3. [Commands](https://github.com/ShubenkoH/weather-api/blob/main/README.md#tada-commands)

## 🔌 Requirements and Preparation

The system requires Docker and Docker Compose for development. It is recommended to use GNU/Linux operating system (Debian, Ubuntu, etc.).

You can configure databse settings in env.dev and in docker-compose.

## 🛠 Installation

1. Open the command prompt.
2. Run `make init`
3. You are great, and can start working.

## 🎉 Commands

* `make init` — Initialize development environment.
* `make up` — Start development environment.
* `make down` — Stop development environment.
* `make makemigrations` — Create new migrations.
* `make migrate` — Apply migrations.
* `make runtests` — Run tests
