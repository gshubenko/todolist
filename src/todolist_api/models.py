import os

from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _


class StatusModel(models.Model):
    name = models.CharField(_('Name'), max_length=256)

    base_ids = [1, 2, 3]

    @property
    def can_change(self):
        return not self.pk in self.base_ids

    def __str__(self) -> str:
        return f"{self.name}"

    class Meta:
        verbose_name = _("Status")
        verbose_name_plural = _("Statuses")


class TaskModel(models.Model):
    title = models.CharField(verbose_name=_('Title'), max_length=256)
    description = models.TextField(
        verbose_name=_('Description'),
        max_length=2000
    )
    status = models.ForeignKey(
        to=StatusModel,
        on_delete=models.PROTECT,
        related_name="tasks",
        verbose_name=_("Status")
    )
    author = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.CASCADE,
        related_name="tasks",
        verbose_name=_("Author")
    )
    performers = models.ManyToManyField(
        to=get_user_model(),
        blank=True,
        related_name="performers_task",
        verbose_name=_("Performers")
    )
    created_at = models.DateTimeField(
        verbose_name=_("created_at"),
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=_("updated_at"),
        auto_now=True,
    )

    def __str__(self) -> str:
        return f"Task({self.pk}-{self.title})"

    class Meta:
        verbose_name = _("Task")
        verbose_name_plural = _("Tasks")


class TaskImageModel(models.Model):
    image = models.ImageField(verbose_name=_("Image"))
    task = models.ForeignKey(
        to=TaskModel,
        on_delete=models.CASCADE,
        related_name="images",
        verbose_name=_("Task")
    )

    def __str__(self) -> str:
        return f"Image>{self.task}"

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")

    def delete(self, using=None, keep_parents=False):
        try:
            os.remove(self.image.path)
        except FileNotFoundError:
            pass
        return super(TaskImageModel, self).delete(
            using=None,
            keep_parents=False
        )

class CommentModel(models.Model):
    comment = models.TextField(verbose_name=_("Comment"), max_length=2000)
    author = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.CASCADE,
        related_name="task_comments",
        verbose_name=_("Author")
    )
    task = models.ForeignKey(
        to=TaskModel, 
        on_delete=models.CASCADE,
        related_name="comments", 
        verbose_name=_("Task")
        )
    parent_comment = models.ForeignKey(
        to="self", 
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="child_comments", 
        verbose_name=_("Parent comment"),
        )
    created_at = models.DateTimeField(
        verbose_name=_("created_at"),
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        verbose_name=_("updated_at"),
        auto_now=True,
    )


    def __str__(self) -> str:
        return f"Comment({self.pk})>{self.author}>{self.task}"

    class Meta:
        verbose_name = _("Comment")
        verbose_name_plural = _("Comment")