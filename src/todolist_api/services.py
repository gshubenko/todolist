from .serializers import TaskSerializer, UpdateTaskStatusSerizlizer
from rest_framework.exceptions import PermissionDenied
from django.utils.translation import gettext_lazy as _
from.models import TaskModel
from .utils import send_email_massage

def update_task(user, task, data, partial) -> dict:
    serializer = None
    if task.author == user:
        serializer = TaskSerializer(task, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer
    elif user in task.performers.all() and not data.get("status") in (1,3):
        serializer = UpdateTaskStatusSerizlizer(
            task, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return TaskSerializer(serializer.instance)
    else:
        raise PermissionDenied(_("You do not have permission to change task"))


def delete_task(task, user):
    if task.author == user:
        task.delete()
    else:
        raise PermissionDenied("You do not have permission to delete task")

def get_task_emails(task: TaskModel) -> list:
    """Returns all emails from Task"""
    emails = [task.author.email]
    emails.extend([i.email for i in task.performers.all()])
    return list(set(emails))

def send_task_create_email(request, task):
    send_email_massage(
            emails=get_task_emails(task), 
            template_path="email_template.html", 
            email_subject="Task created", 
            context=dict(
                url=f"{request.build_absolute_uri()}{task.pk}/",
                action="create",
                title="Task created",
                task_title=task.title,
            ), 
            request=request)

def send_task_updated_email(request, task):
    send_email_massage(
            emails=get_task_emails(task), 
            template_path="email_template.html", 
            email_subject="Task updated", 
            context=dict(
                url=f"{request.build_absolute_uri()}",
                action="updated",
                title="Task updated",
                task_title=task.title,
            ),
            request=request)
