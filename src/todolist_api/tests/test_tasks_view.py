from ..serializers import (
    UserSerializer,
    CommentSerializer,
    StatusSerializer,
    TaskSerializer,
    UpdateTaskStatusSerizlizer
)
from ..models import (StatusModel, TaskModel, TaskImageModel, CommentModel)
import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth import get_user_model
from django.core.management import call_command
import logging

logger = logging.getLogger(__name__)


class CreateTaskTest(APITestCase):
    def setUp(self):
        self.user = self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.data = {
            "title": "test titel",
            "description": "test description",
            "status": 1,
            "author": self.user.pk,
            "performers": [
                self.user.pk
            ]
        }

    def test_can_create_user(self):
        response = self.client.post(
            reverse('tasks-list'), self.data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadTaskTest(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.data = dict(
            title="test titel",
            description="test description",
            status_id=1,
            author=self.user)
        self.task = TaskModel.objects.create(**self.data)

    def test_can_read_statuses_list(self):
        response = self.client.get(reverse('tasks-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_statuses_detail(self):
        response = self.client.get(
            reverse('tasks-detail', args=[self.task.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateTaskTest(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.data = dict(
            title="test titel",
            description="test description",
            status=1,
            author=self.user.pk
        )
        self.task = TaskModel.objects.create(**dict(
            title="test titel",
            description="test description",
            status_id=1,
            author=self.user
        ))

    def test_can_update_other_tasks(self):
        response = self.client.put(
            reverse('tasks-detail', args=[self.task.pk]), self.data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_update_self_task(self):
        self.client.logout()
        self.client.force_authenticate(user=self.user)
        response = self.client.put(
            reverse('tasks-detail', args=[self.task.pk]), self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DeleteTaskTest(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.task = TaskModel.objects.create(**dict(
            title="test titel",
            description="test description",
            status_id=1,
            author=self.user
        ))

    def test_can_delete_other_task(self):
        response = self.client.delete(
            reverse('tasks-detail', args=[self.task.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_delete_self_task(self):
        self.client.logout()
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(
            reverse('tasks-detail', args=[self.task.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
