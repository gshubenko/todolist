from ..serializers import (
    UserSerializer,
    CommentSerializer,
    StatusSerializer,
    TaskSerializer,
    UpdateTaskStatusSerizlizer
)
from ..models import (StatusModel, TaskModel, TaskImageModel, CommentModel)
import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth import get_user_model
from django.core.management import call_command
import logging

logger = logging.getLogger(__name__)


class CreateCommentTest(APITestCase):
    def setUp(self):
        self.user = self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.user)
        self.task = TaskModel.objects.create(**dict(
            title="test titel",
            description="test description",
            status_id=1,
            author=self.user
        ))
        self.task2 = TaskModel.objects.create(**dict(
            title="test titel",
            description="test description",
            status_id=1,
            author=self.superuser
        ))
        self.data = {
            "comment": "string",
            "author": self.user.pk,
            "parent_comment": None
        }

    def test_can_create_comment(self):
        response = self.client.post(
            reverse('task_comments-list', args=[self.task.pk]), self.data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_cannot_create_comment(self):
        response = self.client.post(
            reverse('task_comments-list', args=[self.task2.pk]), self.data, format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ReadCommentTest(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.task = TaskModel.objects.create(**dict(
            title="test titel",
            description="test description",
            status_id=1,
            author=self.user
        ))
        self.comment = CommentModel.objects.create(
            task=self.task,
            author=self.user,
            comment="11111111",
        )

    def test_can_read_comment_list(self):
        response = self.client.get(reverse('task_comments-list', args=[self.task.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_comment_detail(self):
        response = self.client.get(
            reverse('task_comments-detail', args=[self.task.pk, self.comment.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)