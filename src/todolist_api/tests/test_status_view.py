import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth import get_user_model
from django.core.management import call_command
import logging

logger = logging.getLogger(__name__)

from ..models import (StatusModel, TaskModel, TaskImageModel, CommentModel)
from ..serializers import (
    UserSerializer,
    CommentSerializer,
    StatusSerializer,
    TaskSerializer,
    UpdateTaskStatusSerizlizer
)

class CreateStatusTest(APITestCase):
    def setUp(self):
        self.user = self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.data = {"name": "Test status"}


    def test_can_create_user(self):
        response = self.client.post(reverse('statuses-list'), self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadStatusTest(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.base_status = StatusModel.objects.get(pk=1)

    def test_can_read_statuses_list(self):
        response = self.client.get(reverse('statuses-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_statuses_detail(self):
        response = self.client.get(reverse('statuses-detail', args=[self.base_status.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateStatusTest(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.data = {"name": "Test status update"}
        self.base_status = StatusModel.objects.get(pk=1)
        self.status = StatusModel.objects.create(name="New test")

    def test_can_update_base_status(self):
        response = self.client.put(reverse('statuses-detail', args=[self.base_status.pk]), self.data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_can_update_status(self):
        response = self.client.put(reverse('statuses-detail', args=[self.status.pk]), self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DeleteStatusTest(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username="user",
            password="user",
            email="user@localhost.com",
        )
        self.superuser = get_user_model().objects.create(
            username="admin",
            password="admin",
            email="admin@localhost.com",
            is_staff=True,
        )
        call_command("loaddata", "status")
        self.client.force_authenticate(user=self.superuser)
        self.base_status = StatusModel.objects.get(pk=1)
        self.status = StatusModel.objects.create(name="New test")

    def test_can_delete_base_status(self):
        response = self.client.delete(reverse('statuses-detail', args=[self.base_status.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_can_delete_status(self):
        response = self.client.delete(reverse('statuses-detail', args=[self.status.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
