from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied
from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_serializer_method
from django.utils.translation import gettext_lazy as _


from .models import (
    StatusModel,
    TaskModel,
    TaskImageModel,
    CommentModel
)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            'username',
            'email',
            'id',
            "first_name",
            "last_name",
        )


class CommentSerializer(serializers.ModelSerializer):
    replys = serializers.SerializerMethodField()

    @swagger_serializer_method(
        serializers.ListField(child=serializers.JSONField()))
    def get_replys(self, obj):
        return CommentSerializer(obj.child_comments.all(), many=True).data

    def validate(self, attrs):
        user = self.context["request"].user
        if not user in attrs["task"].performers.all() and\
                user != attrs["task"].author:
            raise PermissionDenied(_('You do not have permission to write comment here.'))

        return super().validate(attrs)

    class Meta:
        model = CommentModel
        fields = "__all__"


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatusModel
        fields = "__all__"

    def validate(self, attrs):
        if self.instance and not self.instance.can_change:
            raise PermissionDenied(_("You do not have permission to change base statuses"))
        return super().validate(attrs)


class TaskImageCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskImageModel
        fields = ("id", "image")


class TaskImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskImageModel
        fields = "__all__"


class TaskSerializer(serializers.ModelSerializer):
    image_url = serializers.SerializerMethodField('get_image_url')
    comments = serializers.SerializerMethodField('get_comments')

    def get_comments(self, obj):
        return CommentSerializer(obj.comments.all(), many=True).data

    def get_image_url(self, obj):
        images = obj.images.all()
        return TaskImageSerializer(images, many=True).data

    class Meta:
        model = TaskModel
        fields = "__all__"

    def to_representation(self, instance: TaskModel):
        representation = super(
            TaskSerializer, self).to_representation(instance)
        representation["status"] = StatusSerializer(instance.status).data
        representation["author"] = UserSerializer(instance.author).data
        representation["performers"] = UserSerializer(
            instance.performers, many=True).data
        return representation


class TaskResponseSerializer(TaskSerializer):
    """Used only for documentation"""
    status = StatusSerializer()
    author = UserSerializer()
    performers = UserSerializer(many=True)
    comments = CommentSerializer(many=True)


class UpdateTaskStatusSerizlizer(TaskSerializer):
    class Meta:
        model = TaskModel
        fields = ("status",)
