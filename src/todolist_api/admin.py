from django.contrib import admin
from . import models as app_models


@admin.register(app_models.StatusModel)
class StatusAdmin(admin.ModelAdmin):
    list_display = ("id", "name")


@admin.register(app_models.TaskModel)
class TaskAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "status", "author",
                    "created_at", "updated_at")


@admin.register(app_models.TaskImageModel)
class ImageAdmin(admin.ModelAdmin):
    list_display = ("id", "task")

@admin.register(app_models.CommentModel)
class CommentAdmin(admin.ModelAdmin):
    list_display = ("id", "task", "author")
