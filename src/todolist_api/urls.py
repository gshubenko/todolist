from django.urls import path, include
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register("statuses", views.StatusViewSet, basename="statuses")
router.register(r"tasks/(?P<task_id>\d+)/images",
                views.TaskImageView, basename="task_images")
router.register(r"tasks/(?P<task_id>\d+)/comments",
                views.CommentViewSet, basename="task_comments")


urlpatterns = router.urls

urlpatterns += [
    path("tasks/", views.TaskCreateListView.as_view(), name="tasks-list"),
    path("tasks/<int:pk>/", views.TaskRetriveUpdateDelete.as_view(),
         name="tasks-detail"),
]
