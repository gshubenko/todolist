from celery import shared_task
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
from django.conf import settings

import logging

logger = logging.getLogger(__name__)


@shared_task
def send_email(emails, html, subject):
    text = strip_tags(html)
    email_massage = EmailMultiAlternatives(
        subject, text, from_email=settings.DEFAULT_FROM_EMAIL ,to=emails)
    email_massage.attach_alternative(html, "text/html")
    email_massage.send()
    logger.info(f"Massage sended to email {' '.join(emails)}")
