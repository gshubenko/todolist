from django.template import loader
from django.conf import settings

from todolist_api.tasks import send_email
import logging

logger = logging.getLogger(__name__)



def send_email_massage(emails, template_path, email_subject, context, request):
    template = loader.get_template(template_path)
    html = template.render(context, request)
    if not settings.TESTING:
        send_email.delay(emails, html, email_subject)
    logger.info("Sent email task created.")