from rest_framework import generics, mixins, status
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.response import Response
from rest_framework import permissions as rest_permissions
from drf_yasg.utils import swagger_auto_schema
from rest_framework.exceptions import PermissionDenied


from todolist.custom_exeptions import Http400APIException
from .models import (
    StatusModel,
    TaskModel,
    TaskImageModel,
    CommentModel,
)
from .serializers import (
    StatusSerializer,
    TaskSerializer,
    TaskResponseSerializer,
    TaskImageSerializer,
    TaskImageCreateSerializer,
    CommentSerializer,
)
from .permissions import IsAdminUserOrReadOnly, IsImageUserOrReadOnly
from .services import (
    update_task,
    delete_task,
    get_task_emails,
    send_task_create_email,
    send_task_updated_email)
from .utils import send_email_massage


class StatusViewSet(ModelViewSet):
    f"""View for status manipulating.
    Statuses with id {StatusModel.base_ids} only read.
    """
    serializer_class = StatusSerializer
    queryset = StatusModel.objects.all()
    permission_classes = (IsAdminUserOrReadOnly,
                          rest_permissions.IsAuthenticated)

    def perform_destroy(self, instance: StatusModel):
        if instance.can_change:
            return super().perform_destroy(instance)
        else:
            raise PermissionDenied("You can't delete a base statuses")


class TaskCreateListView(
    generics.GenericAPIView,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
):
    """"""
    queryset = TaskModel.objects.select_related("author", "status").\
        prefetch_related("performers").all()
    serializer_class = TaskSerializer

    @swagger_auto_schema(responses={
        200: TaskResponseSerializer
    })
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @swagger_auto_schema(responses={
        201: TaskResponseSerializer
    })
    def post(self, request, *args, **kwargs):
        self.request.data["author"] = self.request.user.pk
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        send_task_create_email(request=request, task=serializer.instance)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class TaskRetriveUpdateDelete(
    generics.GenericAPIView,
    mixins.RetrieveModelMixin,
):
    queryset = TaskModel.objects.select_related("author", "status").\
        prefetch_related("performers")
    serializer_class = TaskSerializer
    permission_classes = (rest_permissions.IsAuthenticated,)

    @swagger_auto_schema(responses={200: TaskResponseSerializer})
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    @swagger_auto_schema(responses={200: TaskResponseSerializer})
    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    @swagger_auto_schema(responses={200: TaskResponseSerializer})
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        delete_task(task=instance, user=self.request.user)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = update_task(
            user=request.user,
            task=instance,
            data=request.data,
            partial=partial
        )

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        send_task_updated_email(request=request, task=instance)

        return Response(serializer.data)


class TaskImageView(
        GenericViewSet,
        mixins.CreateModelMixin,
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.DestroyModelMixin,
):
    queryset = TaskImageModel.objects.select_related("task").all()
    serializer_class = TaskImageSerializer
    permission_classes = (
        rest_permissions.IsAuthenticated,
        IsImageUserOrReadOnly,
    )

    def get_queryset(self):
        return self.queryset.filter(task_id=self.kwargs["task_id"])

    @swagger_auto_schema(request_body=TaskImageCreateSerializer)
    def create(self, request, *args, **kwargs):
        request.data["task"] = self.kwargs["task_id"]
        return super().create(request, *args, **kwargs)


class CommentViewSet(
        GenericViewSet,
        mixins.CreateModelMixin,
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin):
    serializer_class = CommentSerializer 
    queryset = CommentModel.objects.select_related(
        "author",
        "parent_comment",
        "task"
    )

    def get_queryset(self):
        return self.queryset.filter(task_id=self.kwargs["task_id"], parent_comment__isnull=True)

    def create(self, request, *args, **kwargs):
        request.data["author"] = self.request.user.pk
        request.data["task"] = self.kwargs["task_id"]
        return super().create(request, *args, **kwargs)