from django.apps import AppConfig
from django.db.utils import ProgrammingError
from django.core.management import call_command
import logging

logger = logging.getLogger(__name__)


class TodolistApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'todolist_api'

    def ready(self):
        from .models import StatusModel
        try:
            if StatusModel.objects.filter(
                pk__in=StatusModel.base_ids).count() != 3:
                call_command("loaddata", "status")
                logger.info("Statuses successfully loaded")
        except ProgrammingError:
            logger.info("Can't get status")
