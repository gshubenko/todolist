import django_filters

from .models import TaskModel

class ProductFilter(django_filters.FilterSet):

    class Meta:
        model = TaskModel
        fields = ['status', 'status__name']