from rest_framework.exceptions import APIException

class Http400APIException(APIException):
    status_code = 400